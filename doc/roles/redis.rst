redis
=====

description
  A persistent caching system, key-value and data structures database

website
  http://redis.io/

license
  BSD

dependencies
------------

- common

variables
---------

.. code-block:: json
   'redisConfig' {
    // required: no
    // description: password of database instance
    'password': 'password'
   }
